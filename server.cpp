#define _WIN32_WINNT 0x501
#include <WS2tcpip.h>
#include <winsock2.h>
#include"server.h"
#include <iostream>
#pragma comment (lib, "ws2_32.lib")

tcp_server::tcp_server(int port) {
      tcp_server:port = port;
}

tcp_server::~tcp_server() {}

tcp_server::start_listening() {

      // Initialize WinSocket.
      WSADATA wsdata;
      WORD ver = MAKEWORD(1, 0);

      // WSAStartup() initialized WinSocket for DLL.
      int ws_fine = WSAStartup(ver, &wsdata);

      if (ws_fine != 0) {
        cerr << "[-] Could not initialize Winsock."<<endl;
      }
      else {
        cout << "[+] Winsock succesfully initialized."<<endl;
      }

      /* SOCKET is simply a UINT, created because
 on Unix sockets are file descriptors(UINT) but not in windows
 so new type SOCKET was created */

    sSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (sSock == INVALID_SOCKET) {
        cerr << "[-] Socket could not be initialized." << endl;
        WSACleanup();
    }
    else {
      cout << "[+] Socket succesfully initialized." << endl;
    }

    // Bind the Socket to IP/Port.
    sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(4000); //HTONS = Host To Network Short.
    addr.sin_addr.s_addr = inet_addr("127.0.0.1"); //Can also use inet_pton.

    bind(sSock, (sockaddr*)&addr, sizeof(addr));
    if (bind == 0) {
        cerr << "[-] Socket could not be binded." << endl;
        WSACleanup();
    }
    else {
      cout << "[+] Socket succesfully binded to " << addr.sin_addr.s_addr;
      cout << "at port " << addr.sin_port << endl;
    }

    // Set Socket to Listening Mode.
    listen (sSock, 10);
    cout << "[+] Socket is listening for incoming connections." << endl;

    // Accept Incoming Connections.
    acceptConns();

    return 0;
}

tcp_server::acceptConns() {

    sockaddr_in client;
    int client_size = sizeof(client);

    while (1) {

        char buffer[4096];
        cSock = accept(sSock, (sockaddr*)&client, &client_size);
        cout << "[+] Connected to " << inet_ntoa(client.sin_addr) << endl;
        cout << "[+] Client's IP is " << inet_ntoa(client.sin_addr) << endl;
        //int recieved_msg = recv(cSock, buffer, 4096, 0);
        //cout << "Client: " << recieved_msg;
        //send(cSock, buffer, strlen(buffer) 0);
        closesocket(cSock);
    }

    closesocket(sSock);
    WSACleanup();
    return 0;
}
