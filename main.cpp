// Project: TCP Server Implementation In C++.
// Architects: Mrinal Wahal & Ashwin Gupta.
// Purpose: Semester 3 Open-Ended Project.

// Required Notice:
      // Requires MinGW Compiler on Windows and GPP on *nix.
      // Add '-lws2_32' in terminal arguments while compiling.


#include "server.h"
#include <iostream>

using namespace std;

int main() {

    tcp_server s = tcp_server(8000);
    s.start_listening();
    return 0;

}
