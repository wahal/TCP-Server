#include <string.h>
#include <winsock2.h>
#include <WS2tcpip.h>
#include <iostream>
#pragma comment (lib, "ws2_32.lib")


using namespace std;

class tcp_server {

      public:
          tcp_server(int);

          // Design a Virtual Class in order to avoid Ambiguity.
          virtual ~tcp_server();

          // Put the server socket in listening mode.
          int start_listening();

      protected:
          int port;
          SOCKET sSock; // Server Socket
          SOCKET cSock; // Client Socket

      private:

          // Accept Connections.
          int acceptConns();

      };
